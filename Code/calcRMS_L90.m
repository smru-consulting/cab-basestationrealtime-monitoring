function [L90rms, t05, t95] = calcRMS_L90(calData, varargin)
% Calculate the level 90 of a calibrated signal

%For impact pile driving, characterize overall dBrms levels by...
% intcgrating sound for each wavefoml  across 90% of thc acoustic energy in
% each wave (using  the 5-95 percentiles to establish the 90%, criterion)
% and avemging across all waves in the pile-driving event (i.e., as
% demonstrated in Figure I of !>.Iadscn ct aL 2006). 

% https://app.box.com/file/710269300528?s=a0ox8isck5yub2h62ocgksmuz4pa1g0e
% Energy = 1/T*sum(Ei*dt)

% Energy
rho= 1026;
c =1500;
insInten =  (calData.^2)/(2*rho*c^2); 
% Cumulative Intensity
cumInten = cumsum(insInten);

% Cumulative intensity limits

cumSumLims = [sum(insInten)*.05 sum(insInten)*.95];
t05 = find(cumSumLims(1)<cumInten,1);
t95= find(cumSumLims(2)<=cumInten,1);

% 
 %Find peak intensity
 [~,  peakIn] = max(insInten);
% 
% 
% % Upper time limit and lower time limit
% t95 = peakIn + find(cumsum(insInten(peakIn:end))> sum(insInten(peakIn:end))*.95,1);
 t95= min([t95, length(calData)]);
% t05 = find(cumsum(insInten(1:peakIn))>= sum(insInten(1:peakIn-1))*.05,1 );
 t05= max([t05, 1]);

L90rms = sqrt(mean(calData(t05:t95).^2));

if ~isempty(varargin)
    close(figure(500))
    figure (500)
    % Make a plot
    subplot(2,1,1)
    plot(calData); hold on;
    scatter(peakIn, max(calData), 'b', 'filled')
    scatter(t05, max(calData), 'r', 'filled')
    scatter(t95, max(calData), 'r', 'filled')
    
    
    subplot(2,1,2)
    plot(cumInten)
    xline(t95, 'r')
    xline( peakIn, 'b')
    xline(t05, 'r')
    hold off
end

end