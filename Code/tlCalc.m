function [tl_series] = tlCalc(input_sys)
%calculate transmission loss (TL) between near and far field CABs


%user variables
%input near/far distances from excel (m)
D1 = 115; %near field
D2 = 457; %far field

%minimum time for near/far clicks to be correlated (sec)
minTime = 1;

%%
%find and discard any false/unmatched clicks

input_sys.Hyd(1).time_sec = input_sys.Hyd(1).arrivalTime*86400;
input_sys.Hyd(2).time_sec = input_sys.Hyd(2).arrivalTime*86400;

%define empty arrays to put corrected inputs
nearRMScorr = [];
farRMScorr = [];
nearTimesCorr = [];
farTimesCorr = [];
tl_series = [];

%if nearField smaller...
if size(input_sys.Hyd(1).time_sec,2) <= size(input_sys.Hyd(2).time_sec,2)
    for i = 1:size(input_sys.Hyd(1).time_sec,2)
        timeDiffs = input_sys.Hyd(2).time_sec-input_sys.Hyd(1).time_sec(i);
        [val,ind] = min(abs(timeDiffs));
        if abs(val) < minTime
            nearRMScorr = [nearRMScorr input_sys.Hyd(1).SELrms(i)];
            farRMScorr = [farRMScorr input_sys.Hyd(2).SELrms(ind)];
            nearTimeCorr = [nearTimesCorr input_sys.Hyd(1).time_sec(i)];
            farTimeCorr = [farTimesCorr input_sys.Hyd(2).time_sec(ind)];
            tl = (input_sys.Hyd(2).SELrms(ind) - input_sys.Hyd(1).SELrms(i))/(log10(D1)-log10(D2));
            tl_series = [tl_series  tl];
        end
    end
%if farField smaller...
else
    for i = 1:size(farTimesSec,2)
        timeDiffs = nearTimesSec-farTimesSec(i);
        [val,ind] = min(timeDiffs);
        if abs(val) < minTime
            nearRMScorr = [nearRMScorr input_sys.Hyd(1).SELrms(ind)];
            farRMScorr = [farRMScorr input_sys.Hyd(2).SELrms(i)];
            nearTimeCorr = [nearTimesCorr input_sys.Hyd(1).time_sec(ind)];
            farTimeCorr = [farTimesCorr input_sys.Hyd(2).time_sec(i)];
            tl = (input_sys.Hyd(2).SELrms(i) - input_sys.Hyd(1).SELrms(ind))/(log10(D1)-log10(D2));
            tl_series = [tl_series  tl];
        end
    end
end

%%
%calculate transmission loss (TL)
%METHOD 1 - calc TL from RMS medians

%calculate medians for near/far RMS array
% nearRMSmed = median(nearRMScorr);
% farRMSmed = median(farRMScorr);

%convert distances to log scale
% D1log = log10(D1);
% D2log = log10(D2);
% 
% %calc TL (linear regression)
% TL = (mean(farRMScorr-nearRMScorr))/(D2log-D1log);
% 

% %%
% %plot
% figure(1)
% scatter((D1*ones(size(nearRMScorr))),nearRMScorr)
% hold on
% scatter((D2*ones(size(farRMScorr))),farRMScorr)
% plot([D1 D2],[nearRMSmed farRMSmed])
% ylabel('RMS')
% xlabel('distance from pile (m)')
% str = sprintf('TL = %.2f',TL);
% title(str)
% 
% x=1;