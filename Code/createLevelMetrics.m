function [dur, E, SEL, p2p, z2p, rmsLvl] = createLevelMetrics(data, fs,...
    sysGain, T0, sppGroup)

% Function to creat the level metrics for analysis. 
% Input: data (wave form), t05 and t95 time, sample rate, overall system
% gain and reference time


if nargin ==4
    disp(' not filtering data')
else
    disp(['Using ' sppGroup ' filter'])
    data = NOAAweighted(data, fs, sppGroup);
end

[~, t05, t95] = calcRMS_L90(data);
data95 = data(t05:t95);

% Duration of each impulse recorded by the click detector
% Also integration time in Gabrielson (VI-24)
dur = length(data95)/fs;


% Exposure (Pa^6) Gabrielson (VI-24)
% Use this value for combining metrics. When coverting
% to dB, things get 'awkward' so save the headache and
% stick with this.
E = dur* mean(data95.^2);


% Sound Exposure Level T0 is the reference duration
% used to keep the quantity unitless
% Gabrielson (V1-25) /
% ISO 18406. 6.4.2.1.2 Single strike sound exposure level (SELss)
SEL = 10*log10(E/T0)-sysGain;

% Peak to peak
p2p= 20*log10(max(data)+min(data))-...
    sysGain;
% Zero to peak
z2p =  20*log10(max(data))-...
    sysGain;

% RMS
rmsLvl= 20*log10(rms(data))- sysGain;



end
