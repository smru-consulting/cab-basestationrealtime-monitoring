
close all; clear all; clc

%% FILE LOCATIONS VANCOUVER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % MSI FILE PATHS DO NOT MODIFY IF YOU ARE NOT IN VANCOUVER!

% COMMMENT THIS SECTION OUT IF YOU ARE RUNNING ON THE FROM VANCOUVER
% OTHERWISE DO NOT MODIFY




% floc = 'C:\Data\PileDrivingOut\20190712';
% floc = 'C:\Data\CABtestClicks';
% clickFiles = dir(fullfile([floc, '\Click_Detector_Click_Detector_Clicks.pgdf']));
%
% Load the Alarm
cd('C:\BitBucketRepositories\cab-basestationrealtime-monitoring')
[alarmSound, ~] = audioread('alarmSound.mp3');

TimeThresh = 61; % how many seconds should the system continue looking for new files?
% loacation of the PAMGuard Data and the alarm
%floc = 'C:\Data\Hood Canal\Day1\BinrayFromRecordings\20200930'
floc = 'C:\Data\Hood Canal\Day1\RerunData\306\binary\20200930';
floc = 'C:\Data\Hood Canal\Day1\binary\binary';

% END MSI FILE LOCAITON
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FILE LOCATIONS SAN JUAN

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% THIS SECTION IS FOR CAB/SJI FILE LOCATIONS, MODIFY HERE IF WORKING ON THE
% A COMPUTER THAT ISN'T THE VANCOUVER MSI MACHINE

% DO NOT MOIDFY IF YOU ARE NOT IN SAN JUAN

% [alarmSound, ~] = audioread(fullfile(...
%     'C:\Users\CAB Laptop 1\Documents\Seawolf\cab-basestationrealtime-monitoring', 'alarmSound.mp3'));
%
%
% clickFiles = dir(fullfile([floc, '\*.pgdf']));
% floc = 'C:\Users\CAB Laptop 1\Documents\Seawolf\septemberMonitor\binary\20200928 ';
%

% END CAB Laptop/SJI Computer File location
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Calibration Information (from Cornell spreadsheet)
pref = 10e-6; %Reference pressure, 1 uPa
% two values assuming two hydrophones
hydSen = [-183, -183]; %dB re: 1V/�Pa
transGain = [0 0]; % Tansducer gain, voltage
hydPreampGain  = hydSen+transGain; %dB re: 1V/uPa
%hydPreampGain = hydPreampGain+60; %dB re: 1mV/uPa, convert V to mV
adConverterGain =[2 2];
MARUGain = 0; % only using for validation
% Bool for not playing the alarm every time
alarmPlayed =[0 0];
analogGain= hydPreampGain+adConverterGain; %dB re: 1mV/�Pa


cmap = [[0 0 0];colormap(parula(5))]; % colormap
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Overall System Transduction Gain replace with end to end if needed
overallSysGain = analogGain +MARUGain;
overallSysGain = [-212.8 -187.8]; % Volts per uPa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Convert out of dB
overallSysGainuPa = 1./(10.^(overallSysGain/20)*10^6); % Volts per uPa


% Multiplier to go from samples to uPa
% invert to get actual multiplier for 12-bit integer values
%overallSysGainuPa = 1./overallSysGainuPa; %�Pa/bit
overallSysGainPa =overallSysGainuPa*pref;


% Soundrap ete calibration
%% Put on infinite loop
nChan = 2; % number of hcannels
fs =62500; % sample rate (Hz)


% Cumulative exposure alarm threshold (adjusted for TL in the latter secion
% just use the NOAA value here
AlarmThresh = 205;

idx =[1 1];
dir_content =[];
filenames = {};
current_files = {};
endFlag =0;



% Transmission loss coefficient
TLcoef =25;
%UPDATE VALUE ABOVE IF WE HAVE TL ESTIMATED

% Reference range (10 m as per noaa guidelines)
r0 =[10 10];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Range of hydrophone to acoustic source (distance from pile rig)
r1 = [10 488]; % if placed at 10m, then no TL offset required
r1 = [115 457];
r1 = [115 457];
r1 = [457 115]; %swapped chans



%UPDATE VALUES ABOVE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Back calculate transmission loss to update threshold (long form so it can
% be remembered next time)
TL = -TLcoef.*(log10(r0)-log10(r1));


% Threhold for alarm- adjust to account for distance to source
cumSELThresh = AlarmThresh - TL; % add a TL value if not monitoring at 10m



%%

% Create the structure to store the cumulative exposure thresholds, and
% values
SELdB = struct();
SELdB.Hyd(1).thresh = cumSELThresh(1);
SELdB.Hyd(2).thresh = cumSELThresh(2);

% Place to store the cumSEL data
SELdB.Hyd(1).cumSEL = [];
SELdB.Hyd(2).cumSEL = [];

% Place to store the cumSEL data
SELdB.Hyd(1).SEL= [];
SELdB.Hyd(2).SEL = [];

% Place to store the cumSEL data
SELdB.Hyd(1).SPLrms = [];
SELdB.Hyd(2).SPLrms = [];

% Place to store the cumSEL data
SELdB.Hyd(1).strikeDur = [];
SELdB.Hyd(2).strikeDur = [];


% 0 to peak values (abs)
SELdB.Hyd(1).peak2peak = [];
SELdB.Hyd(2).peak2peak = [];
%

% Zero to peak
SELdB.Hyd(1).zero2Peak = [];
SELdB.Hyd(2).zero2Peak = [];

% Exposure and Cumulative exposure refrenced to 1 sec
SELdB.Hyd(1).E = [];
SELdB.Hyd(2).E = [];

SELdB.Hyd(1).cumSEL1sec = [];
SELdB.Hyd(2).cumSEL1sec = [];

SELdB.Hyd(1).SELrms =[];
SELdB.Hyd(2).SELrms =[];

SELdB.Hyd(1).arrivalTime=[];
SELdB.Hyd(2).arrivalTime=[];

SELdB.Hyd(1).clipped=[];
SELdB.Hyd(2).clipped=[];


% Data for ther species
SELPhocid = SELdB; % phocide
SELOtar = SELdB; % Otariadae
SELlF = SELdB; % mysticetes
SELhf = SELdB; % Odontocetes
SELmf = SELdB;



%% Total monitoring duration, cannot exceed XXX CumSEL in 1 hour

T =(35*60); % total seconds of expected pile driving. Garbrielson Notes
T0 =1; % reference duration used to keep SEL quantities dimensionless


%% Run the loop
tic % Start of elapsed time

% Initialize counter for while loop
counter = 1;

% Set up the output figure
fig = figure (1)
movegui(fig,  'southwest');
set(gcf, 'Renderer', 'painters', 'Position', [10 10 900 600])



while true
    % Identify click detector files in the expected location
    dir_content = dir(fullfile([floc, '\*Click_Detector_Click_Detector_Clicks*.pgdf']));
    filenames = {dir_content.name};
    
    % Set diff, identify the new ones
    new_files = setdiff(filenames,current_files);
    
    % If the loop has passed the first iteration and there are new files
    if counter>1 && ~isempty(new_files)
        %Wait three seconds for file to populate
        pause(3);
        %Grab the second to mose recent file
        new_files = current_files(end-1);
    end
    % If there are new files, do the analysis. Otherwise wait.
    if ~isempty(new_files)
        
        
        % New files found!
        fprintf(' new files\n')
        
        % update the list of the files the system knows about
        current_files = filenames;
        
        % Step through the new files, load the data, calibrate it and then
        % calculate the peak and cumulative SPL90
        for ii =1:length(new_files)
            
            % structure for the new file names etc.
            clickFiles = dir(fullfile([floc, '\',new_files{ii}]));
            
            % Minimum file size
            if clickFiles.bytes>201
                [clickdata, fileInfo] = loadPamguardBinaryFile(fullfile(clickFiles.folder,...
                    clickFiles.name));
                
                % If there are data in the PG file, do the analysis
                if ~isempty(clickdata)
                    for jj =1:length(clickdata)
                        
                        
                        
                        chan = clickdata(jj).channelMap;
                        
                        %hexidecimal fix
                        if chan == 8
                            chan = 2;
                        end
                        
                        % Multiply wave data by overall system gain to produce
                        % the pressure in micro pascals. Then don't worry about
                        % hte reference dB (since it's just 1)
                        data = clickdata(jj).wave;
                        
                        
                        % arrival time
                        arrTime =clickdata(jj).date;
                        
                        SELdB.Hyd(chan).arrivalTime(idx(chan)) = arrTime;
                        SELPhocid.Hyd(chan).arrivalTime(idx(chan)) = arrTime;
                        
                        % Raw metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = createLevelMetrics(data,...
                            fs, overallSysGain(chan), T0)
                        
                        SELdB.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELdB.Hyd(chan).E(idx(chan)) = E;
                        SELdB.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELdB.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELdB.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELdB.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        SELdB.Hyd(chan).arrivalTime(idx(chan)) = arrTime;
                        
                        
                        % Phocid metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = createLevelMetrics(data,...
                            fs, overallSysGain(chan), T0, 'phocid')
                        
                        SELPhocid.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELPhocid.Hyd(chan).E(idx(chan)) = E;
                        SELPhocid.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELPhocid.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELPhocid.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELPhocid.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        
                        
                        
                        % Otaridae Metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = ...
                            createLevelMetrics(data, fs, overallSysGain(chan),...
                            T0, 'Otariid')
                        
                        SELOtar.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELOtar.Hyd(chan).E(idx(chan)) = E;
                        SELOtar.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELOtar.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELOtar.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELOtar.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        
                        % Low Frequency Metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = ...
                            createLevelMetrics(data, fs, overallSysGain(chan),...
                            T0, 'LF')
                        
                        SELlF.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELlF.Hyd(chan).E(idx(chan)) = E;
                        SELlF.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELlF.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELlF.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELlF.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        
                        
                        % Mid Frequency Metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = ...
                            createLevelMetrics(data, fs, overallSysGain(chan),...
                            T0, 'MF')
                        
                        SELmf.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELmf.Hyd(chan).E(idx(chan)) = E;
                        SELmf.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELmf.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELmf.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELmf.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        
                        
                        % High Frequency Metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = ...
                            createLevelMetrics(data, fs, overallSysGain(chan),...
                            T0, 'HF')
                        
                        SELhf.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELhf.Hyd(chan).E(idx(chan)) = E;
                        SELhf.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELhf.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELhf.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELhf.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;
                        
                        
                        
                        
                        %Bool for clipped data
                        clipped= max(abs(data)>.99)
                        
                        % Clipping is species agnostic...
                        SELdB.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELPhocid.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELPhocid.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELlF.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELmf.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELhf.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELOtar.Hyd(chan).clipped(idx(chan))= clipped;
                        
                        
                        
                        % Advance the counter
                        idx(chan)=idx(chan)+1;
                        
                        
                        
                    end
                    
                    
                    % Exclude all of the clipped data
                    clipped = SELdB.Hyd(chan).clipped;
                    otherwiseEmpty = isempty(SELdB.Hyd(chan).strikeDur);
                    
                    goodIdxs = ~(clipped.*otherwiseEmpty);
                    
                    % Calculate the cumulative SEL
                    % Pull out exposure (Pa^2)
                    E_values = SELdB.Hyd(chan).E(goodIdxs); %Raw
                    Eval_pho = SELPhocid.Hyd(chan).E(goodIdxs); %phocid
                    Eval_otar = SELOtar.Hyd(chan).E(goodIdxs); %Otaridae
                    Eval_lf = SELlF.Hyd(chan).E(goodIdxs); %Otaridae
                    Eval_mf = SELmf.Hyd(chan).E(goodIdxs); %Otaridae
                    Eval_hf = SELhf.Hyd(chan).E(goodIdxs); %Otaridae
                    
                    % Because the duration of the exposures changes for each
                    % impulse, the weighting factor for each is 1. Gabrielson
                    % (VI-27)
                    Edn = 1* cumsum(E_values);
                    EdnPho = 1* cumsum(Eval_pho);
                    EdnOtar = 1* cumsum(Eval_otar);
                    Ednlf = 1* cumsum(Eval_lf);
                    Ednmf = 1* cumsum(Eval_mf);
                    Ednhf = 1* cumsum(Eval_hf);
                    
                    
                    
                    % Convert to a level, account for the monitoring exposure
                    % time (say, 1hour) and subtract tje calibration values.
                    % Gabrielson (VI-28)
                    
                    % Note coverting to dB is the LAST thing to do
                    GabSEL = 10*log10(Edn/(T))-overallSysGain(chan);
                    SELdB.Hyd(chan).cumSEL1sec = GabSEL;
                    
                    GabSELpho = 10*log10(EdnPho/(T))-overallSysGain(chan);
                    SELPhocid.Hyd(chan).cumSEL1sec = GabSELpho;
                    
                    GabSELotar = 10*log10(EdnOtar/(T))-overallSysGain(chan);
                    SELOtar.Hyd(chan).cumSEL1sec = GabSELotar;
                    
                    GabSELlf = 10*log10(Ednlf/(T))-overallSysGain(chan);
                    SELlF.Hyd(chan).cumSEL1sec = GabSELlf;
                    
                    GabSELmf = 10*log10(Ednmf/(T))-overallSysGain(chan);
                    SELmf.Hyd(chan).cumSEL1sec = GabSELmf;
                    
                    GabSELhf = 10*log10(Ednhf/(T))-overallSysGain(chan);
                    SELhf.Hyd(chan).cumSEL1sec = GabSELhf;
                    
                    % Figure for real-time checking of cumulativeSEL
                    
                    
                    subplot(2,2,1)
                     if ~isempty(SELdB.Hyd(1).cumSEL1sec)
                      
                        
                        hold on
                        h1= plot(1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELdB.Hyd(1).cumSEL1sec, 'Color',cmap(1,:)),
                        
                        h2 =plot(    1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELPhocid.Hyd(1).cumSEL1sec,'Color',cmap(2,:))
                        
                        h3 =plot(    1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELOtar.Hyd(1).cumSEL1sec,'Color',cmap(3,:))
                        
                        h4 =plot(    1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELlF.Hyd(1).cumSEL1sec,'Color',cmap(4,:))
                        
                        h5 =plot(    1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELmf.Hyd(1).cumSEL1sec,'Color',cmap(5,:))
                        
                        h6= plot(    1:length(SELdB.Hyd(1).cumSEL1sec),...
                            SELhf.Hyd(1).cumSEL1sec,'Color',cmap(6,:))
                        
                        
                        
                        yline(cumSELThresh(1), 'r')
                        
                        legend({'Raw', 'Phocid', 'Otariid',...
                            'LF Cet', 'MF Cet.', 'HF cet.',...
                            'Maximum Trheshold'}, 'Location', 'Southeast')
                        else
                        disp('No data yet')
                    end
                    
                    xlabel('Impulse Number')
                    ylabel('Cumulative SEL')
                    title('Cumulative SEL Chan 1')
                    
                    
                    
                    
                     subplot(2,2,2)
                    if ~isempty(SELdB.Hyd(2).cumSEL1sec)
                       
                       
                        hold on
                        h1= plot(1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELdB.Hyd(2).cumSEL1sec, 'Color',cmap(1,:)),
                        
                        h2 =plot(    1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELPhocid.Hyd(2).cumSEL1sec,'Color',cmap(2,:))
                        
                        h3 =plot(    1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELOtar.Hyd(2).cumSEL1sec,'Color',cmap(3,:))
                        
                        h4 =plot(    1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELlF.Hyd(2).cumSEL1sec,'Color',cmap(4,:))
                        
                        h5 =plot(    1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELmf.Hyd(2).cumSEL1sec,'Color',cmap(5,:))
                        
                        h6= plot(    1:length(SELdB.Hyd(2).cumSEL1sec),...
                            SELhf.Hyd(2).cumSEL1sec,'Color',cmap(6,:))
                        hold on
                        yline(cumSELThresh(2), 'r')
                        
                        
                        legend({'Raw', 'Phocid', 'Otariid',...
                            'LF Cet', 'MF Cet.', 'HF cet.',...
                            'Maximum Trheshold'}, 'Location', 'Southeast')
                    else
                        disp('No data yet')
                    end
                    
                    xlabel('Impulse Number')
                    ylabel('Cumulative SEL')
                    title('Cumulative SEL Chan 2')
                    
                    % Play an alarm if the cumulative exposure has been
                    % exceeded (but don't be a jerk about it)
                    if any(SELdB.Hyd(2).cumSEL> -SELdB.Hyd(2).thresh) && alarmPlayed(2)==0
                        sound(alarmSound, fs)
                        alarmPlayed(2)=1;
                    end
                    
                    if any(SELdB.Hyd(1).cumSEL> -SELdB.Hyd(1).thresh) && alarmPlayed(1)==0
                        sound(alarmSound, fs)
                        alarmPlayed=(1);
                    end
                    
                    
                    % Create a figure for real time SEL for 8dB reduction
                    % verification in pseudo-real time
                    
                    
                    figure(1)
                    
                    subplot(2,2,3)
                    hold on
                    xx = 1:idx(1);
                    clippedIdx =SELdB.Hyd(1).clipped;
                    scatter(SELdB.Hyd(1).arrivalTime(~clippedIdx),...
                        SELdB.Hyd(1).SEL(~clippedIdx),'k', 'filled')
                    scatter(SELdB.Hyd(1).arrivalTime(logical(clippedIdx)),...
                        SELdB.Hyd(1).SEL(logical(clippedIdx)),'r', 'filled')
                    ylim([50 230])
                    if size(SELdB.Hyd(1).arrivalTime,2) > 2
                        xlim([min(SELdB.Hyd(1).arrivalTime) max(SELdB.Hyd(1).arrivalTime)])
                    end
                    xlabel('Impulse Arrival Time')
                    ylabel('Pulse SEL')
                    legend({'OK','Clipped'})
                    datetick
                    
                    subplot(2,2,4)
                    hold on
                    xx = 1:idx(2);
                    clippedIdx =SELdB.Hyd(2).clipped;
                    scatter(SELdB.Hyd(2).arrivalTime(~clippedIdx),...
                        SELdB.Hyd(2).SEL(~clippedIdx),'k', 'filled')
                    scatter(SELdB.Hyd(2).arrivalTime(logical(clippedIdx)),...
                        SELdB.Hyd(2).SEL(logical(clippedIdx)),'r', 'filled')
                    if size(SELdB.Hyd(2).arrivalTime,2) > 1
                        xlim([min(SELdB.Hyd(2).arrivalTime) max(SELdB.Hyd(2).arrivalTime)])
                    end
                    xlabel('Impulse Arrival Time')
                    ylabel('Pulse SEL')
                    legend({'OK','Clipped'})
                    datetick
                    
                end
                tic
            end
            
            clickFiles.name
        end
        
        ii;
        
    else
        
        
        % If  nothing has changed in 3 seconds, end
        elspsTime = toc;
        if elspsTime>=TimeThresh
            fprintf('No new files in set period, ending loop\n')
            endFlag =1;
        end
        %fprintf('No new files\n')
        
    end
    
    % End Flag, stop the loop
    if  endFlag
        break
    end
    counter=counter+1;
end

save(['C:\Users\CAB Laptop 1\Documents\Seawolf\septemberMonitor\variable\' datestr(now,'yyyymmdd_HHMMSS') '.mat'],'SELdB');

%% Daily Reporting

% Number of pulses per day (asume hydrophone caught all pluses)
numPulses = [length(SELdB.Hyd(1).SEL) length(SELdB.Hyd(2).SEL)]

%  median single strike SEL
SingleStrikeSEL_median = [median(SELdB.Hyd(1).SEL)...
    median(SELdB.Hyd(2).SEL)]

% Mean single strik SEL
SingleStrikeSEL_mean =[20*log10(mean(10.^(SELdB.Hyd(1).SELrms/20)))...
    20*log10(mean(10.^(SELdB.Hyd(2).SELrms/20)))]

% Pile driving duration in minutes
pileDuration = [max(SELdB.Hyd(1).arrivalTime)-min(SELdB.Hyd(1).arrivalTime)...
    max(SELdB.Hyd(1).arrivalTime)-min(SELdB.Hyd(1).arrivalTime)]*24*60;


% Cumulative SEL over the total monitoring period
newCumulativeSEL = 10*log10(sum(SELdB.Hyd(1).E)/(pileDuration(1)*60)) - ...
    overallSysGain(1)




