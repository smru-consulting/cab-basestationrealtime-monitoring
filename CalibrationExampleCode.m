% Example code for estimating/confirming methodologies for converting wav
% files to PA. Unfinished 2020/09/21

close all;
clear all;
clc

file = 'C:\Users\Kaitlin Palmer\Documents\ForKaitlin\ForKaitlin\Nearfield_clip250Hz.wav'
[yy, fs] = audioread(file);
vRange = 2 % Voltage Range

% Convert matlab units to volts

% Hydrophone (Pressure ) - conversion factor V/Pa [hydrophone sensitivity]
% Hydrophone out voltage

% TRANSDUCTION GAIN

% A/D converter (Voltage) - conversion Bits/Volt 
% Bits returned
matlabCal = -188.8;
bitDepth = 16;


matlabCallinear = 1/(10^(matlabCal/20)*1E-6) % PA/V

% Convert bits to volts
volts = yy

rms_recieved = (20*log10(sqrt(mean(yy.^2))/1))-matlabCal% should be 150dB
rms_received = 20*log10(rms(yy))-matlabCal

10*log10(mean(yy.^2))-matlabCal

