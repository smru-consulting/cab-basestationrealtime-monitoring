%% Load binary files

parent_path = 'C:\Users\CAB Laptop 1\Documents\Seawolf\septemberMonitor';
figure_path = strcat(parent_path,'\postprocessing\figures');
output_data_path = strcat(parent_path,'\postprocessing\results');

%The file location for each PC processed binary
floc = {strcat(parent_path,'\PG rerun\306\binary\20200930')
         strcat(parent_path,'\PG rerun\309\binary\20200930')};

for f=1:length(floc)
    dir_content{f} = dir(fullfile([floc{f}, '\*Click_Detector_Click_Detector_Clicks*.pgdf']));
    filenames{f} = {dir_content{f}.name};
end

%Define sample frequency, sysGain, initialize idx
fs =62500;
overallSysGain = [-212.2 -187.7];
idx = [1 1];
%% Total monitoring duration, cannot exceed XXX CumSEL in 1 hour

T =(35*60); % total seconds of expected pile driving. Garbrielson Notes
T0 =1; % reference duration used to keep SEL quantities dimensionless 
%% Import GPS data from SQL
%SQL query-- 
%SELECT Name,StreamerIndex,UTC,Latitude,Longitude 
%FROM HydrophoneStreamer
%Save as <parent_path>\fieldNotes\GPSData.csv

dataPath = strcat(parent_path,'\fieldNotes\GPSData.csv');

[~, ~, raw] = xlsread(dataPath);

nRow = size(raw,1);
nCol = size(raw,2);
header = raw(1,:);   % Store header information
raw(1,:) = [];

BuoyGPS = struct;
for ii=1:length(floc)
    BuoyGPS.Hyd(ii).lat = [];
    BuoyGPS.Hyd(ii).long = [];
    BuoyGPS.Hyd(ii).time = [];
end

for r = 1:nRow-1
    if raw{r,4}~=0 && raw{r,5}~=0 && raw{r,2}<2
        BuoyGPS.Hyd(raw{r,2}+1).lat(length(BuoyGPS.Hyd(raw{r,2}+1).lat)+1) = raw{r,4};
        BuoyGPS.Hyd(raw{r,2}+1).long(length(BuoyGPS.Hyd(raw{r,2}+1).long)+1) = raw{r,5};
        BuoyGPS.Hyd(raw{r,2}+1).time(length(BuoyGPS.Hyd(raw{r,2}+1).time)+1) = raw{r,3}+datenum('30-Dec-1899');
    end
end

%% Load pile coordinates to coordinates structure

dataPath = strcat(parent_path,'\fieldNotes\eventData.csv');

[~, ~, raw] = xlsread(dataPath);

nRow = size(raw,1);
nCol = size(raw,2);
header = raw(1,:);   % Store header information
raw(1,:) = [];

PileEvents = struct;

eventCount = 1;
for r = 1:nRow-1
    PileEvents.PileNumber(eventCount) = raw{r,1};
    if datenum(raw{r,2})<10^5
        PileEvents.start_time(eventCount) = datenum(raw{r,2})+datenum('30-Dec-1899');
        PileEvents.end_time(eventCount) = datenum(raw{r,3})+datenum('30-Dec-1899');
    else
        PileEvents.start_time(eventCount) = datenum(raw{r,2});
        PileEvents.end_time(eventCount) = datenum(raw{r,3});
    end
    PileEvents.lat(eventCount) = raw{r,4};
    PileEvents.long(eventCount) = raw{r,5};
    eventCount = eventCount+1;
end

%% Import PG binary data into SELdB structure
for kk=1:length(filenames)
    for ii=1:length(filenames{kk})
        filenames_kk = filenames{kk};
        clickFiles = dir(fullfile([floc{kk}, '\',filenames_kk{ii}]));
        if ~isempty(clickFiles)
            if clickFiles.bytes>201
                    [clickdata, fileInfo] = loadPamguardBinaryFile(fullfile(clickFiles.folder,...
                        clickFiles.name));
                if ~isempty(clickdata)
                    for jj =1:length(clickdata)
                        chan=kk;
                        
                        % arrival time
                        arrTime =clickdata(jj).date;
                        
                        %Get event index and distance to pile
                        withinEvent = 0;
                        for e=1:length(PileEvents.start_time)
                            if arrTime>=PileEvents.start_time(e) && arrTime<=PileEvents.end_time(e)
                                withinEvent=1;
                                spacialStruct.Hyd(chan).eventIndex(idx(chan)) = e;
                                spacialStruct.Hyd(chan).pileIndex(idx(chan)) = PileEvents.PileNumber(e);
                                pileCoords = [PileEvents.lat(e) PileEvents.long(e)];
                            end
                        end
                        
                        if ~withinEvent
                            continue
                        end
                        
                        
                        format long
                        %Get buoy location and calculate distance
                        buoyDataFound = 0;
                        for r=1:length(BuoyGPS.Hyd(chan).time)-1
                            if arrTime>=BuoyGPS.Hyd(chan).time(r) && arrTime<BuoyGPS.Hyd(chan).time(r+1)
                                buoyCoords = [BuoyGPS.Hyd(chan).lat(r) BuoyGPS.Hyd(chan).long(r)];
                                spacialStruct.Hyd(chan).buoylat(idx(chan)) = buoyCoords(1);
                                spacialStruct.Hyd(chan).buoylong(idx(chan)) = buoyCoords(2);                                
                                spacialStruct.Hyd(chan).distanceToPile(idx(chan)) = lldistkm(pileCoords,buoyCoords)*1000;
                                buoyDataFound = 1;
                            end
                        end
                        
                        if ~buoyDataFound
                            buoyCoords = [spacialStruct.Hyd(chan).buoylat(idx(chan)-1) spacialStruct.Hyd(chan).buoylong(idx(chan)-1)];
                            spacialStruct.Hyd(chan).buoylat(idx(chan)) = buoyCoords(1);
                            spacialStruct.Hyd(chan).buoylong(idx(chan)) = buoyCoords(2);                                
                            spacialStruct.Hyd(chan).distanceToPile(idx(chan)) = lldistkm(pileCoords,buoyCoords)*1000;
                        end

                        % Multiply wave data by overall system gain to produce
                        % the pressure in micro pascals. Then don't worry about
                        % hte reference dB (since it's just 1)
                        data = clickdata(jj).wave;

                        SELdB.Hyd(chan).arrivalTime(idx(chan)) = arrTime;
                        SELPhocid.Hyd(chan).arrivalTime(idx(chan)) = arrTime;

                        % Raw metrics 
                        [dur, E, SEL, p2p, z2p, rmsLvl] = createLevelMetrics(data,...
                            fs, overallSysGain(chan), T0);

                        SELdB.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELdB.Hyd(chan).E(idx(chan)) = E;
                        SELdB.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELdB.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELdB.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELdB.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;


                        % Phocid metrics 
                        [dur, E, SEL, p2p, z2p, rmsLvl] = createLevelMetrics(data,...
                             fs, overallSysGain(chan), T0, 'phocid');

                        SELPhocid.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELPhocid.Hyd(chan).E(idx(chan)) = E;
                        SELPhocid.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELPhocid.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELPhocid.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELPhocid.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;



                        % Otaridae Metrics
                        [dur, E, SEL, p2p, z2p, rmsLvl] = ...
                            createLevelMetrics(data, fs, overallSysGain(chan),...
                            T0, 'Otariid');

                        SELOtar.Hyd(chan).strikeDur(idx(chan)) = dur;
                        SELOtar.Hyd(chan).E(idx(chan)) = E;
                        SELOtar.Hyd(chan).SEL(idx(chan)) = SEL;
                        SELOtar.Hyd(chan).peak2peak(idx(chan)) = p2p;
                        SELOtar.Hyd(chan).zero2Peak(idx(chan)) = z2p;
                        SELOtar.Hyd(chan).SPLrms(idx(chan)) =rmsLvl;




                        %Bool for clipped data
                        clipped= max(abs(data)>.99);

                        % Clipping is species agnostic... 
                        SELdB.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELPhocid.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELPhocid.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELlF.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELmf.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELhf.Hyd(chan).clipped(idx(chan)) = clipped;
                        SELOtar.Hyd(chan).clipped(idx(chan))= clipped;

                        % Advance the counter
                        idx(chan)=idx(chan)+1;
                            
                    end
                end
            end
        end
    end
end

%% Return list of pairs
pair = struct;

pair_idx = 1;

pairless_near = [];
pairless_far = [];

%Loop through near hydrophone arrival times
for ii=1:length(SELdB.Hyd(1).arrivalTime)
    near_time = SELdB.Hyd(1).arrivalTime(ii);
    %Grab the nearest pile in time for far hydrophone
    compare_times = SELdB.Hyd(2).arrivalTime - near_time;
    [val,jj] = min(abs(compare_times));
    %If the difference in time between the two are within a certain
    %threshold define them as a pair
    if val<.0005
        pair.near_index(pair_idx) = ii;
        pair.far_index(pair_idx) = jj;
        pairless_near(ii) = 0;
        pairless_far(jj) = 0;
        pair_idx = pair_idx+1;
    %If the near hydrophone click does not have a distinct pair, attach it
    %to a pairless list of indexes
    else
        pairless_near(ii) = 1;
    end
end

%Make a pairless list of indexes for the far hydrophone
for jj=1:length(SELdB.Hyd(2).arrivalTime)
    near_time = SELdB.Hyd(2).arrivalTime(jj);
    compare_times = SELdB.Hyd(1).arrivalTime - near_time;
    [val,ii] = min(abs(compare_times));
    if val>=.0005
        pairless_far(jj) = 1;
    end
end

%% Plot the transmission loss coeffiecient for the paired set

%init
tl_series = [];
tl_time_series = [];
to_remove = [];

%Loop through list of pairs (defined above)
for ii=1:length(pair.near_index)
    %Use distance data from GPS locations (defined in click data loop) to
    %calculate TLcoeff
    tlcoeff = (SELdB.Hyd(2).SEL(pair.far_index(ii)) - SELdB.Hyd(1).SEL(pair.near_index(ii)))/...
        (log10(spacialStruct.Hyd(2).distanceToPile(pair.far_index(ii)))-log10(spacialStruct.Hyd(1).distanceToPile(pair.near_index(ii))));
    if tlcoeff<-15 && tlcoeff>-40
        tl_series = [tl_series tlcoeff];
        tl_time_series = [tl_time_series SELdB.Hyd(2).arrivalTime(pair.far_index(ii))];
        pairless_near(pair.near_index(ii)) = 0;
    %If the transmission loss coefficient is outside of a range, throw out
    %the values and add them to the list of strikes without a pair
    else
         pairless_far(pair.far_index(ii)) = 1;
         pairless_near(pair.near_index(ii)) = 1;
         to_remove = [to_remove ii];
    end
end

pair.near_index(to_remove) = [];
pair.far_index(to_remove) = [];  
%% Loop through pairless and determine if false positive or true

remove_from_pairless_near = [];
for ii=1:length(pairless_near)
    if pairless_near(ii)==1
        st = ii-2;
        if st<1
            st = 1;
        end
        if ii+2<=length(SELdB.Hyd(1).arrivalTime)
            arr_time_neighborhood = SELdB.Hyd(1).arrivalTime(st:ii+2);
            SELrms_neighborhood = SELdB.Hyd(1).SEL(st:ii+2);
            in_time = min(abs(arr_time_neighborhood-arr_time_neighborhood(3)))<.00003;
            in_line = max(abs(SELrms_neighborhood-SELrms_neighborhood(3)))<10;
            if in_time && in_line
               pairless_near(ii) = 0;
            end
    %         figure(6)
    %         scatter(arr_time_neighborhood,SELrms_neighborhood)
    %         x=1
        end
    end
end

for ii=1:length(pairless_far)
    if pairless_far(ii)==1
        if ii+2<=length(SELdB.Hyd(2).arrivalTime) && ii-2>1
            start = ii-2;
            e = ii+2;
        else
            start = 1
            e = 5
        end
        arr_time_neighborhood = SELdB.Hyd(2).arrivalTime(start:e);
        SELrms_neighborhood = SELdB.Hyd(2).SEL(start:e);
        in_time = min(abs(arr_time_neighborhood-arr_time_neighborhood(3)))<.00003;
        in_line = max(abs(SELrms_neighborhood-SELrms_neighborhood(3)))<10;
        if in_time && in_line
           pairless_far(ii) = 0;
        end
    end
%     figure(6)
%     scatter(arr_time_neighborhood,SELrms_neighborhood)
%     x=1
end

%% Get metrics and export data
output = struct;
in = [];
source_level_10 = 0;
source_leve_1 = 0;
for ii=1:eventCount
    for kk=1:length(floc)
        if ii == eventCount
            inEvent.Hyd(kk).is_in = ones(1,length(spacialStruct.Hyd(kk).eventIndex));
            pile_index = ['Total' 'Total'];
        else
            for jj=1:length(spacialStruct.Hyd(kk).eventIndex)
                if spacialStruct.Hyd(kk).eventIndex(jj) == ii
                    inEvent.Hyd(kk).is_in(jj) = 1;
                else
                    inEvent.Hyd(kk).is_in(jj) = 0;
                end
            end
            pile_index = spacialStruct.Hyd(kk).pileIndex(logical(inEvent.Hyd(kk).is_in));
        end
    end
    
    times = SELdB.Hyd(2).arrivalTime(logical(inEvent.Hyd(2).is_in));
    
    output.PileEvents.Event{ii}.Attributes.EventNumber = ii;
    output.PileEvents.Event{ii}.Pile_Number = pile_index(1);
    output.PileEvents.Event{ii}.start_t = times(1);
    output.PileEvents.Event{ii}.end_t = times(end);
    output.PileEvents.Event{ii}.duration = (times(end)-times(1))*60*60*24;
    
    tl_in = 1:length(tl_series);
    for tl =1:length(tl_in)
        if tl_time_series(tl)>=min(times) && tl_time_series(tl)<=max(times)
            tl_in(tl) = 1;
        else
            tl_in(tl) = 0;
        end
    end
    tl_coeffs = tl_series(logical(tl_in));
    
    output.PileEvents.Event{ii}.median_tl_coeff = median(tl_coeffs);
    for kk=1:length(floc)
        in = logical(inEvent.Hyd(kk).is_in);

        d = spacialStruct.Hyd(kk).distanceToPile(in);
        SEL = SELdB.Hyd(kk).SEL(in);  
        oPeak = SELdB.Hyd(kk).zero2Peak(in);
        TL10m = -1*median(tl_coeffs)*(log10(10)-log10(mean(d)));
        SPLrms = SELdB.Hyd(kk).SPLrms(in);
        
        output.PileEvents.Event{ii}.channels.Hyd{kk}.Attributes.BuoyID = kk;
        if kk==1
            output.PileEvents.Event{ii}.channels.Hyd{kk}.Attributes.BuoyLoc = 'near';
        elseif kk==2
            output.PileEvents.Event{ii}.channels.Hyd{kk}.Attributes.BuoyLoc = 'far';
        end
        output.PileEvents.Event{ii}.channels.Hyd{kk}.strikeCount = length(SPLrms);
        
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.distance = mean(d);
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.median0Peak = median(oPeak);
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.arithMeanSEL = 20*log10(mean(10.^(SEL/20)));
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.medianSEL = median(SEL);
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.maxSEL = max(SEL);
        rCUMSEL = 20*log10(mean(10.^(SEL/20)))+ 10*log10(966);
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.cumSEL = rCUMSEL;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.maxSPLrms = max(SPLrms);
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{1}.TL10m = TL10m;
        
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.distance = 10;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.median0Peak = median(oPeak)-TL10m;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.arithMeanSEL = 20*log10(mean(10.^(SEL/20)))-TL10m;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.medianSEL = median(SEL)-TL10m;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.medianSPLrms = median(SPLrms)-TL10m;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.medianSPLrms = median(SPLrms)-TL10m;
        source_level_10 = rCUMSEL-TL10m;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{2}.cumSEL = rCUMSEL-TL10m;

        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.distance = 1;
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.median0Peak = median(oPeak)-median(tl_coeffs)*log10(mean(d));
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.arithMeanSEL = 20*log10(mean(10.^(SEL/20)))-median(tl_coeffs)*log10(mean(d));
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.medianSEL = median(SEL)-median(tl_coeffs)*log10(mean(d));
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.medianSPLrms = median(SPLrms)-median(tl_coeffs)*log10(mean(d));
        source_level_1 = rCUMSEL - median(tl_coeffs)*log10(mean(d));
        output.PileEvents.Event{ii}.channels.Hyd{kk}.range.r{3}.cumSEL = source_level_1;
    end
end


output_t = struct2xml(output,strcat(output_data_path,'\output.xml'));
fid = fopen(strcat(output_data_path,'\results.xml'), 'wt' );
for r=1:size(output_t,1)
        fprintf(fid,'%s\n',output_t(r,:));
end

fclose(fid);

%% Create CAB drift plots
drift=[];
for chan=1:2
    drift{chan} = [];
    start_coords = [spacialStruct.Hyd(chan).buoylat(1) spacialStruct.Hyd(chan).buoylong(1)];
    for i=1:length(spacialStruct.Hyd(chan).buoylat)
        cur_coords = [spacialStruct.Hyd(chan).buoylat(i) spacialStruct.Hyd(chan).buoylong(i)];
        dist_from_start = lldistkm(start_coords,cur_coords)*1000;
        drift{chan} = [drift{chan} dist_from_start];
    end
end

figure(1)
fig1 = figure('units','inch','position',[0,0,15,10]);
set(gca, 'FontName', 'Arial')
subplot(2,1,1)
scatter(SELdB.Hyd(1).arrivalTime,drift{1},'filled')
hold on
scatter(SELdB.Hyd(2).arrivalTime,drift{2},'filled')
datetick
xlabel('Time (UTC)','FontSize',15)
ylabel('CAB Distance From X0 (m)','FontSize',15)
title('CAB Drift','FontSize',20)
legend('306','309')

subplot(2,1,2)
scatter(SELdB.Hyd(1).arrivalTime,spacialStruct.Hyd(1).distanceToPile,'filled')
hold on
scatter(SELdB.Hyd(2).arrivalTime,spacialStruct.Hyd(2).distanceToPile,'filled')
datetick
xlabel('Time (UTC)','FontSize',15)
ylabel('Distance to source (m)','FontSize',15)
title('CAB Distance to Source','FontSize',20)
legend('306','309')

print(gcf,strcat(figure_path,'\BuoyLocations.png'),'-dpng','-r200'); 

%% Create cab drift including pile events

figure(5)
fig1 = figure('units','inch','position',[0,0,15,10]);
set(gca, 'FontName', 'Arial')
subplot(3,2,1)
scatter(SELdB.Hyd(1).arrivalTime(~pairless_near),SELdB.Hyd(1).SEL(~pairless_near), 'filled')
ylabel({'Pulse SEL' ; '(dB re 1$\mu Pa^{2}\cdot s$)'},'Interpreter','LaTEX','FontName', 'Arial','FontSize',15)
xlabel('Arrival Time (UTC)','FontSize',10,'Interpreter','LaTEX')
title('Near','FontSize',25,'Interpreter','LaTEX')
datetick
subplot(3,2,3)
scatter(SELdB.Hyd(1).arrivalTime,spacialStruct.Hyd(1).distanceToPile,'filled')
ylabel({'CAB distance'; 'From source (m)'},'FontSize',15,'Interpreter','LaTEX')
xlabel('Arrival Time (UTC)','FontSize',10,'Interpreter','LaTEX')
datetick
subplot(3,2,5)
scatter(spacialStruct.Hyd(1).distanceToPile,SELdB.Hyd(1).SEL,'filled')
xlabel('Distance from source (m)','Interpreter','LaTEX','FontSize',10)
ylabel({'Pulse SEL' ; '(dB re 1$\mu Pa^{2}\cdot s$)'},'Interpreter','LaTEX','FontName', 'Arial','FontSize',15)

subplot(3,2,2)
scatter(SELdB.Hyd(2).arrivalTime(~pairless_far),SELdB.Hyd(2).SEL(~pairless_far), 'filled')
title('Far','FontSize',25,'Interpreter','LaTEX')
xlabel('Arrival Time (UTC)','FontSize',10,'Interpreter','LaTEX')
datetick
subplot(3,2,4)
scatter(SELdB.Hyd(2).arrivalTime,spacialStruct.Hyd(2).distanceToPile,'filled')
xlabel('Arrival Time (UTC)','FontSize',10,'Interpreter','LaTEX')
datetick
subplot(3,2,6)
scatter(spacialStruct.Hyd(2).distanceToPile,SELdB.Hyd(2).SEL,'filled')
xlabel('Distance from source (m)','Interpreter','LaTEX')



%% Create TL Plot
figure(2)
fig2 = figure('units','inch','position',[0,0,15,10]);
set(gca, 'FontName', 'Arial')
scatter(tl_time_series,tl_series,'filled')
xlabel('Arrival Time (UTC)','FontSize',15)
ylabel('TLCoeff','FontSize',15)
title('Transmission Loss Coefficient','FontSize',20)
datetick

print(gcf,strcat(figure_path,'\TLCoeffPlot.png'),'-dpng','-r200');  

%% Create TL over space plot
figure(3)
fig3 = figure('units','inch','position',[0,0,15,10]);
set(gca, 'FontName', 'Arial')

scatter(spacialStruct.Hyd(1).distanceToPile,SELdB.Hyd(1).SEL)
hold on
scatter(spacialStruct.Hyd(2).distanceToPile,SELdB.Hyd(2).SEL)
hold on
xx = 1:max(spacialStruct.Hyd(2).distanceToPile);
yy = source_level_10  +median(tl_series)*log10(xx);
plot(xx(10:end),yy(10:end))
ylabel('Pulse SEL (dB re 1$\mu Pa^{2}\cdot s$)','Interpreter','LaTEX','FontName', 'Arial','FontSize',15)
xlabel('Distance from source (m)','Interpreter','LaTEX','FontName', 'Arial','FontSize',15)
title('Pulse SEL over sapce','Interpreter','LaTEX','FontSize',20,'FontName', 'Arial')

print(gcf,strcat(figure_path,'\TLSpacePlot.png'),'-dpng','-r200');  

%% Plot pulseSEL x time
fig4 = figure('units','inch','position',[0,0,15,10]);
set(gca, 'FontName', 'Arial')
figure(4)
subplot(2,1,1)
scatter(SELdB.Hyd(1).arrivalTime(~pairless_near),SELdB.Hyd(1).SEL(~pairless_near), 'filled')
ylabel({'Pulse SEL' ; '(dB re 1$\mu Pa^{2}\cdot s$)'},'Interpreter','LaTEX','FontName', 'Arial','FontSize',15)
datetick
xlabel('Arrival Time (UTC)','Interpreter','LaTEX','FontSize',15,'FontName', 'Arial')
title('Near CAB','Interpreter','LaTEX','FontSize',20,'FontName', 'Arial')
subplot(2,1,2)
scatter(SELdB.Hyd(2).arrivalTime(~pairless_far),SELdB.Hyd(2).SEL(~pairless_far), 'filled')
xlabel('Arrival Time (UTC)','Interpreter','LaTEX','FontSize',15,'FontName', 'Arial')
ylabel({'Pulse SEL' ; '(dB re 1$\mu Pa^{2}\cdot s$)'},'Interpreter','LaTEX','FontSize',15,'FontName', 'Arial')
title('Far CAB','Interpreter','LaTEX','FontSize',20,'FontName', 'Arial')
datetick

print(gcf,strcat(figure_path,'\PulseLevels.png'),'-dpng','-r200');    